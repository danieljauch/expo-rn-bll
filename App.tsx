import React from "react"
import { FlatList, View } from "react-native"
import { NativeRouter as Router, Switch, Route } from "react-router"

import { indexRecipes } from "./src/model/queries/recipe"

import IndexRecipes from "./src/view/recipes"

export default function AppMain() {
	return (
		<Router>
			<Switch>
				<View>
					<Route
						path="/"
						render={() => {
							const recipes = await indexRecipes()
							return <IndexRecipes recipes={recipes} />
						}}
					/>
				</View>
			</Switch>
		</Router>
	)
}
