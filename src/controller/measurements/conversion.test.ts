import { convert } from "conversion"

describe("convert()", () => {
	it("converts tablespoon to teaspoon properly", () => {
		const measurement = {
			amount: 1,
			unit: "tbsp"
		}

		expect(convert(meansurement, "tsp").amount).toBe(3)
	})

	it("converts tablespoon to cup properly", () => {
		const measurement = {
			amount: 16.231,
			unit: "tbsp"
		}

		expect(convert(meansurement, "cup").amount).toBe(1)
	})

	it("converts teaspoon to tablespoon properly", () => {
		const measurement = {
			amount: 3,
			unit: "tsp"
		}

		expect(convert(meansurement, "tbsp").amount).toBe(1)
	})

	it("converts teaspoon to cup properly", () => {
		const measurement = {
			amount: 48.692,
			unit: "tsp"
		}

		expect(convert(meansurement, "cup").amount).toBe(1)
	})

	it("converts cup to tablespoon properly", () => {
		const measurement = {
			amount: 1,
			unit: "cup"
		}

		expect(convert(meansurement, "tbsp").amount).toBe(48.692)
	})

	it("converts cup to teaspoon properly", () => {
		const measurement = {
			amount: 1,
			unit: "cup"
		}

		expect(convert(meansurement, "tsp").amount).toBe(16.231)
	})
})
