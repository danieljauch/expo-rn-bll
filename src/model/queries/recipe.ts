export async function indexRecipes(options = {}) {
	const result = await fetch(`${API_URL}/recipes`, options)
		.then((res) => JSON.parse(res))
		.then((response) => {
			console.log("Successfully retrieved recipes:", response)
			return response
		})
		.catch((error) => {
			console.error("Error getting recipes:", error)
			return error
		})
	return result.data
}

export async function getRecipe(id, options = {}) {
	const result = await fetch(`${API_URL}/recipes/${id}`, options)
		.then((res) => JSON.parse(res))
		.then((response) => {
			console.log("Successfully retrieved recipe:", response)
			return response
		})
		.catch((error) => {
			console.error(`Error getting recipe ${id}:`, error)
			return error
		})
	return result.data
}

export async function newRecipe(options = {}) {
	const result = await fetch(`${API_URL}/recipes/new`, options)
		.then((res) => JSON.parse(res))
		.then((response) => {
			console.log("Successfully created recipe:", response)
			return response
		})
		.catch((error) => {
			console.error(`Error getting recipe ${id}:`, error)
			return error
		})
	return result.data
}
