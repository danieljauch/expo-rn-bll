import { indexRecipes, getRecipe, newRecipe } from "recipe"

describe("indexRecipes()", () => {
	it("gets all recipes in the database", () => {
		const result = await indexRecipes()

		expect(result).toEqual(expect.arrayContaining([]))
	})
})

describe("getRecipe()", () => {
	it("gets a specific recipe from the database", () => {
		const result = await getRecipe("1")

		expect(result).toEqual(expect.objectContaining({}))
	})
})

describe("newRecipe()", () => {
	it("adds a new recipe to the database", () => {
		const result = await newRecipe()

		expect(result).toEqual(expect.objectContaining({}))
	})
})
