import MeasurementType from "./measurement"

export default interface IngredientType {
	measurement: MeasurementType
	name: string
}
