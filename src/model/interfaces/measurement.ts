import UnitType from "./unit"

export default interface MeasurementType {
	amount: number
	unit: UnitType
}
