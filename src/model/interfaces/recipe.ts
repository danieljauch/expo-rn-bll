import { IngredientType, InstructionType } from "./index"

export default interface RecipeType {
	ingredients: IngredientType[]
	instructions: InstructionType[]
}
