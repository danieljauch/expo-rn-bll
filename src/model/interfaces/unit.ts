enum UnitType {
	TABLESPOON = "tbsp",
	TEASPOON = "tsp",
	CUP = "cup"
}

export default UnitType
