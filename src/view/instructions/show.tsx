import React from "react"
import { StyleSheets, Text, View } from "react-native"

import { InstructionType } from "../../model/interfaces"
import { color } from "../theme"

const styles = StyleSheet.create({
	stepNumber: {
		display: "inline",
		color: color.blue,
		paddingRight: 8,
		fontWeight: "bold"
	},
	text: {
		display: "inline-block"
	}
})

interface Props {
	instruction: InstructionType
}

export default function ShowInstruction({ stepNumber, text }: Props) {
	return (
		<View>
			<Text style={styles.stepNumber}>{stepNumber}:</Text>
			<Text style={styles.text}>{text}</Text>
		</View>
	)
}
