import React from "react"
import { StyleSheets, TextInput, View } from "react-native"

import { InstructionType } from "../../model/interfaces"
import { color } from "../theme"

const styles = StyleSheet.create({
	stepNumber: {
		display: "inline",
		color: color.blue,
		paddingRight: 8,
		fontWeight: "bold"
	},
	text: {
		display: "inline-block"
	}
})

interface Props {
	instruction: InstructionType
}

export default function NewInstruction({ stepNumber, text }: Props) {
	return (
		<View>
			<TextInput style={styles.stepNumber} value={stepNumber} />
			<TextInput style={styles.text} value={text} />
		</View>
	)
}
