import React from "react"
import { FlatList, StyleSheets, Text, View, Button } from "react-native"

import { RecipeType } from "../../model/interfaces"
import { newRecipe } from "../../model/queries/recipe"

import { font, color } from "../theme"
import NewInstruction from "../instructions/new"
import NewIngredient from "../ingredients/new"

const styles = StyleSheet.create({
	heading: {
		color: color.blue,
		fontSize: font.size.heading
	},
	divider: {
		height: 0,
		borderTopColor: color.blue,
		borderTopWidth: 2,
		marginVertical: 8
	}
})

export default function NewRecipe({ instructions, ingredients }: RecipeType) {
	return (
		<View>
			<Text style={styles.heading}>Instructions:</Text>
			<FlatList data={instructions} renderItem={(instruction) => <NewInstruction {...instruction} />} />

			<View style={styles.divider} />

			<Text style={styles.heading}>Ingredients:</Text>
			<FlatList data={ingredients} renderItem={(ingredient) => <NewIngredient {...ingredient} />} />

			<Button onClick={newRecipe}>Add recipe</Button>
		</View>
	)
}
