import React from "react"
import { FlatList, View } from "react-native"

import { RecipeType } from "../../model/interfaces"
import NewRecipe from "./new"
import ShowRecipe from "./show"

interface Props {
	recipes?: RecipeType[]
}

export default function IndexRecipes({ recipes }: Props) {
	return (
		<View>
			<FlatList data={recipes || []} renderItem={(recipe) => <ShowRecipe {...recipe} />} />
			<NewRecipe />
		</View>
	)
}
