import React from "react"
import { FlatList, StyleSheets, Text, View } from "react-native"

import { RecipeType } from "../../model/interfaces"
import { font, color } from "../theme"
import ShowInstruction from "../instructions/show"
import ShowIngredient from "../ingredients/show"

const styles = StyleSheet.create({
	heading: {
		color: color.blue,
		fontSize: font.size.heading
	},
	divider: {
		height: 0,
		borderTopColor: color.blue,
		borderTopWidth: 2,
		marginVertical: 8
	}
})

export default function ShowRecipe({ instructions, ingredients }: RecipeType) {
	return (
		<View>
			<Text style={styles.heading}>Instructions:</Text>
			<FlatList data={instructions} renderItem={(instruction) => <ShowInstruction {...instruction} />} />

			<View style={styles.divider} />

			<Text style={styles.heading}>Ingredients:</Text>
			<FlatList data={ingredients} renderItem={(ingredient) => <ShowIngredient {...ingredient} />} />
		</View>
	)
}
