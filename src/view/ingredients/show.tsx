import React from "react"
import { StyleSheets, Text } from "react-native"

import { IngredientType } from "../../model/interfaces"
import { color } from "../theme"

const styles = StyleSheet.create({
	ingredient: {
		padding: 4,
		borderBottomColor: color.green,
		borderBottomWidth: 2,
		borderBottomStyle: "dashed"
	}
})

export default function ShowIngredient({ measurement, name }: IngredientType) {
	return (
		<Text style={styles.ingredient}>
			{measurement} {name}
		</Text>
	)
}
