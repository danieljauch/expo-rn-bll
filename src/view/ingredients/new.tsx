import React from "react"
import { StyleSheets, TextInput, View } from "react-native"

import { IngredientType } from "../../model/interfaces"
import { color } from "../theme"

const styles = StyleSheet.create({
	ingredient: {
		padding: 4,
		borderBottomColor: color.green,
		borderBottomWidth: 2,
		borderBottomStyle: "dashed"
	}
})

export default function NewIngredient({ measurement, name }: IngredientType) {
	return (
		<View style={styles.ingredient}>
			<TextInput value={measurement} />
			<TextInput value={name} />
		</View>
	)
}
